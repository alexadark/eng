<?php

include_once( 'lib/init.php' );

// Include Beans. Do not remove the line below.
require_once( get_template_directory() . '/lib/init.php' );

include_once( 'lib/functions/autoload.php' );

function wst_get_items( $item_field, $item_view_path ) {
	$items = carbon_get_the_post_meta( $item_field, 'complex' );
	if ( ! $items ) {
		return;
	}
	foreach ( $items as $item ) {
		include( ( $item_view_path ) );

	}

}




