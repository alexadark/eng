<?php
beans_remove_attribute('beans_main','class','uk-block');
//beans_remove_attribute('beans_widget_panel[_testimonials][_text]','class','uk-panel-box');
beans_remove_attribute('beans_post','class','uk-panel-box');

beans_add_smart_action( 'beans_main_prepend_markup', 'wst_services_area' );
function wst_services_area() {
	include 'lib/structure/views/services-view.php';
	}

beans_add_smart_action( 'beans_main_prepend_markup', 'wst_testimonials_area' );
function wst_testimonials_area() {
	include 'lib/structure/views/testimonials-view.php';
	}

beans_load_document();