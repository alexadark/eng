<?php
beans_add_smart_action('wp','wst_set_up_header_structure');
function wst_set_up_header_structure() {

	beans_remove_action( 'beans_site_title_tag' );

	//sticky header
	beans_add_attribute( 'beans_header_wrapper', 'data-uk-sticky', "{top:-300, animation:'uk-animation-slide-top'}" );

	beans_wrap_markup( 'beans_header', 'beans_header_wrapper', 'div', array(
		'class' => 'tm-header-wrapper',
	) );

	beans_add_smart_action( 'beans_header_wrapper_append_markup', 'wst_sub_header' );
	function wst_sub_header () {
		include 'views/sub_header_view.php';
		}

	// Breadcrumb
	beans_remove_action( 'beans_breadcrumb' );
	beans_add_smart_action( 'beans_header_wrapper_after_markup', 'wst_hero_area' );
	function wst_hero_area() {
		include 'views/hero-area-view.php';
		}
		
//		off canvas
	beans_remove_attribute('beans_primary_menu_offcanvas_button','class','uk-button');
	beans_remove_output('beans_offcanvas_menu_button');
}
