<?php
$image_id = esc_attr(carbon_get_the_post_meta('crb_hero_image'));
$image_url = $image_id ? wp_get_attachment_image_url($image_id, 'full'): CHILD_IMG . 'hero-default.jpg';
$heading = carbon_get_the_post_meta('crb_hero_heading');
$excerpt = carbon_get_the_post_meta('crb_hero_excerpt');
$link = esc_attr(carbon_get_the_post_meta('crb_hero_link'));
?>
<div class="tm-hero-area "
style = "background: url(<?php echo $image_url?>)no-repeat;
	-webkit-background-size:cover ;background-size: cover;
	">
	<div class="tm-hero-content uk-container uk-container-center">
		<?php if($heading){?>
		<h1 class="tm-hero-heading uk-heading-large">
			<?php echo $heading; ?>
		</h1>
		<?php }?>
		<?php if($excerpt){?>
		<p class="tm-hero-excerpt">
			<?php echo $excerpt; ?>
		</p>
		<?php }?>
		<?php if($link){?>
		<a href="<?php echo $link?>"
		   class="uk-button">Read More</a>
		<?php }?>
	</div>
</div>
