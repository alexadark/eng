<?php
$icon = wp_get_attachment_image($item['crb_service_icon'],'full');
$title = esc_attr($item['crb_service_title']);
$link = esc_attr ($item['crb_service_link']);

?>
<div class="service-item ">
	<div class="service-icon"><?php echo $icon;?></div>

	<a href="<?php echo $link;?>">
		<span class="service-name uk-text-center"><?php echo $title;?></span>
	</a>
</div>