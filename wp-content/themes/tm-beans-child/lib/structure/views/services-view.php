<?php
$link = esc_html(carbon_get_the_post_meta('crb_services_link'));
?>

<section class="tm-services uk-block">
	<div class="uk-container uk-container-center">
		<h2 class="services-title uk-text-center">Services</h2>

		<div class="services-icons uk-grid uk-grid-width-1-2 uk-grid-width-medium-1-4">
			<?php
			wst_get_items('crb_services','lib/structure/views/service-item-view.php');?>
		</div>
		<a href="<?php echo $link;?>"
		   class="more-link uk-text-center">More Services ></a>
	</div>


</section>


