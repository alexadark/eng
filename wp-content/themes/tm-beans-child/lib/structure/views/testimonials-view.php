<?php
$bg = wp_get_attachment_image_url(carbon_get_the_post_meta('crb_testimonials_bg'),'full');
?>
<section class="tm-testimonials-section">
	<div class="uk-grid uk-grid-width-medium-1-2">
		<div class="testimonials-text">
			<div class="section-title uk-text-center">testimonials</div>
			<?php echo beans_widget_area('testimonials');?>
		</div>
		<div class="testimonials-image"
		style="background: url(<?php echo $bg;?>)no-repeat;"></div>

	</div>
</section>