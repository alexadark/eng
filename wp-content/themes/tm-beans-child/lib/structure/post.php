<?php

beans_add_smart_action('wp','wst_set_up_post_structure');
function wst_set_up_post_structure(){
	//Remove title only on pages
	if ( is_page() ) {
		beans_remove_action( 'beans_post_title' );
	}
	if(is_page('partners')){
		beans_remove_attribute('beans_main','class','uk-block');
	}
	add_action( 'beans_main_after_markup', 'wst_display_builder' );
	function wst_display_builder() {
		$layouts = carbon_get_the_post_meta( 'crb_block_layouts', 'complex' );
		foreach ( $layouts as $layout ) {
			$function_name = 'wst_display' . $layout['_type'];
			if ( function_exists( $function_name ) ) {
				call_user_func( $function_name, $layout );

			}
		}
	}

}


