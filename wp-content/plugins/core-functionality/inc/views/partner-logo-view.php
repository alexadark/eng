<?php $logo_id = esc_attr( $item['crb_partner_logo'] );
$logo          = wp_get_attachment_image( $logo_id, 'full' );
$link          = esc_attr( $item[ 'crb_partner_link' ] );
?>
<div class="partner-logo">
	<?php if ( $link ){ ?>
	<a href="<?php echo $link; ?>"
	   target="_blank">
		<?php } ?>
			<?php echo $logo; ?>
		<?php if ( $link ){ ?>
	</a>
<?php } ?>
</div>
