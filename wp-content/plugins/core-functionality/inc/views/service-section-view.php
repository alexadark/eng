<?php
$classes = $layout['crb_class'];
$title   = esc_attr( $layout['crb_service_title'] );
$text    = esc_attr( $layout['crb_service_text'] );
$features = $layout['crb_service_features'];
$link    = esc_attr( $layout['crb_service_link'] );

 ?>
<div class="tm-service-section uk-block <?php echo $classes?>">
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="tm-service uk-width-medium-2-5  ">
				<h3 class="section-title"><?php echo $title; ?></h3>
				<p class="service-content"><?php echo $text;?></p>
			</div>
			<div class="tm-features uk-width-medium-2-5 uk-push-1-5">
				<?php echo $features;?>
				<a href="<?php echo $link; ?>"
				   class="more-link">Learn More ></a>
			</div>
		</div>
	</div>

</div>
