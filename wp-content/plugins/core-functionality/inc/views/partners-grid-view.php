<div class="tm-partners-grid uk-container uk-container-center">
	<div class="uk-grid uk-grid-collapse uk-grid-width-1-2 uk-grid-width-medium-1-3">
		<?php wst_get_layout_items($layout,'crb_partners','views/partner-logo-view.php'); ?>
	</div>
</div>