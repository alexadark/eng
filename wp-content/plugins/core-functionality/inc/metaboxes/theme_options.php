<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', 'Theme Options' )
         ->add_fields( array(
	         Field::make('checkbox','crb_css_dev_mode')
		         ->set_option_value('yes')

         ) );