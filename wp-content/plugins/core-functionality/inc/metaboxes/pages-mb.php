<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', 'Hero Image' )
         ->show_on_post_type( array( 'page' ) )
         ->add_fields( array(
	         Field::make( 'image', 'crb_hero_image', __( 'Hero Image', CHILD_TEXT_DOMAIN ) ),
	         Field::make( 'text', 'crb_hero_heading', __( 'Hero Heading', CHILD_TEXT_DOMAIN ) ),
	         Field::make( 'textarea', 'crb_hero_excerpt', __( 'Hero Excerpt', CHILD_TEXT_DOMAIN ) )
	              ->set_rows( 5 ),
	         Field::make( 'text', 'crb_hero_link', __( 'Hero Link', CHILD_TEXT_DOMAIN ) ),
         ) );

Container::make( 'post_meta', 'Services' )
         ->show_on_post_type( array( 'page' ) )
         ->show_on_page( (int) get_option( 'page_on_front' ) )
         ->add_fields( array(
	         Field::make( 'complex', 'crb_services' )->set_layout( 'tabbed' )
	              ->add_fields( array(
		              Field::make( 'image', 'crb_service_icon', __( 'Services Icon', CHILD_TEXT_DOMAIN ) ),
		              Field::make( 'text', 'crb_service_title', __( 'Service Title', CHILD_TEXT_DOMAIN ) ),
		              Field::make( 'text', 'crb_service_link', __( 'Service Link', CHILD_TEXT_DOMAIN ) ),
	              ) ),
	         Field::make( 'text', 'crb_services_link', __( 'Services Link', CHILD_TEXT_DOMAIN ) ),
         ) );

Container::make( 'post_meta', 'Testimonials Section' )
         ->show_on_post_type( array( 'page' ) )
         ->show_on_page( (int) get_option( 'page_on_front' ) )
         ->add_fields( array(
	         Field::make( 'image', 'crb_testimonials_bg', __( 'Testimonials SEction Background Image', CHILD_TEXT_DOMAIN ) ),
         ) );