<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

$classes          = Field::make( 'text', 'crb_class' );
$service_features = Field::make( 'rich_text', 'crb_service_features' );
$service_title    = Field::make( 'text', 'crb_service_title' );
$service_text     = Field::make( 'textarea', 'crb_service_text' )->set_rows( 5 );
$service_link     = Field::make( 'text', 'crb_service_link' );
$benefit_bg       = Field::make( 'image', 'crb_benefit_bg', 'benefit_background' );

Container::make( 'post_meta', 'layouts' )
         ->show_on_post_type( 'page' )
         ->add_fields( array(
	         Field::make( 'complex', 'crb_block_layouts' )->set_layout('tabbed')
		         /**
		          * SERVICES
		          */

		          ->add_fields( 'service_section', array(
			         $classes,
			         $service_title,
			         $service_text,
			         $service_features,
			         $service_link
		         ) )
		         /**
		          * BENEFIT
		          */
		         ->add_fields( 'benefit_section', array(
			         $benefit_bg,
			         $service_title,
			         $service_text,
		         ) )
	         /**
	          * PARTNERS
	          */
			->add_fields('partners_grid', array(
				Field::make('complex','crb_partners')->set_layout('tabbed')
		         ->add_fields(array(
			         Field::make('image', 'crb_partner_logo'),
			         Field::make( 'text', 'crb_partner_link' )
		         ))

	         ))
         ));